package teamaos.easyway;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;

public class MapsActivity extends AppCompatActivity {

    private TextView toolbartxt;
    private ImageView favs;
    private RecyclerView maps_recycler;
    private MapView maps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        toolbartxt = findViewById(R.id.toolbarTXT);
        favs = findViewById(R.id.favourites_maps);
        maps_recycler = findViewById(R.id.maps_recycler);
        maps = findViewById(R.id.mapView);


    }
}
