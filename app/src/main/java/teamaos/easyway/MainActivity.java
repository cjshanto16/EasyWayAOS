package teamaos.easyway;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView notifications,advertisement,favourites;
    private TextView tooltext;
    private RecyclerView places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        notifications = findViewById(R.id.notifications);
        advertisement = findViewById(R.id.advertisement);
        favourites = findViewById(R.id.favourites);
        tooltext = findViewById(R.id.toolbarTXT);



        places = findViewById(R.id.places_recyclerview);






        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,MapsActivity.class));
            }
        });

    }
}
