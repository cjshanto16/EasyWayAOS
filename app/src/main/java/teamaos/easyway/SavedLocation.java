package teamaos.easyway;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

public class SavedLocation extends AppCompatActivity {

    private RecyclerView saved_location_recycler;
    private Toolbar saved_toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_location);
        saved_toolbar = findViewById(R.id.saved_location_toolbar);
        setSupportActionBar(saved_toolbar);

        saved_location_recycler = findViewById(R.id.saved_location_recyclerview);
    }
}
